#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>

const char *MyFs::MYFS_MAGIC = "MYFS";

int base_headers = 0;
int base_inode_table = 512;
int base_names = 512 * 10;
int base_data = 512 * 15;
//disk memory: header -> inodesTblde -> vector -> root


MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) {
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

	//format();
	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION)) {
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
	else
		getFilesData();
}

void MyFs::format() {

	// put the header in place
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	header.numBlocks = 2048;   //1 mega byte(1024 * 1024) / sizeBlock(512) = 2048
	header.sizeBlock = BLOCK_SIZE;

	blkdevsim->write(0, sizeof(header), (const char*)&header);

	int sizeTable = 512 * 3;
	inodeTable.len = 0;
	blkdevsim->write(base_inode_table, sizeof(inodeTable), (const char*)&inodeTable);

	all_names.len = 0;

	// TODO: put your format code here
}

void MyFs::create_file(std::string path_str, bool directory) {
	if(directory)  //not a file
		throw std::runtime_error("not implementeds");
	int addr = getFirstEmpty();
	char not_empty = '1';
	blkdevsim->write(addr, 1, &not_empty);

	strcpy(all_names.allNames[all_names.len].name,path_str.c_str());
	all_names.allNames[all_names.len].inode = inodeTable.len;
	all_names.allNames[all_names.len].is_dir = directory ? 1 : 0;
	all_names.allNames[all_names.len].file_size = 0;
	all_names.len++;
	blkdevsim->write(base_names, sizeof(all_names), (const char*)&all_names);    //update names

	inode_entry newInode;
	newInode.blocks[0] = addr / 512;
	newInode.numBlocks = 1;
	inodeTable.table[inodeTable.len] = newInode;
	inodeTable.len++;
	blkdevsim->write(base_inode_table, sizeof(inodeTable), (const char*)&inodeTable);    //update inode table

}

//function reads data in root of the parameter path_str
std::string MyFs::get_content(std::string path_str) {
	int foundName = 0;
	int index = -1;
	int size = 0;
	for(int i = 0; i < all_names.len; i++){
		if(path_str == all_names.allNames[i].name){   //found name
			foundName = 1;
			index = all_names.allNames[i].inode;
			size = all_names.allNames[i].file_size;
			break;
		}
	}
	if(foundName == 0)
		throw std::runtime_error("file not exist!");
	int addr = inodeTable.table[index].blocks[0] * 512;;
	char content[size];
	blkdevsim->read(addr, size, (char*)&content);
	std::string ret = std::string(content);
	return ret;
}

//function sets the content parameter in root with saving the file name
void MyFs::set_content(std::string path_str, std::string content) {
	int foundName = 0;
	if(content.length() > 511)
		throw std::runtime_error("can't contain over 1 block of memorty(maximum 511 bytes)");
	int index = -1;
	for(int i = 0; i < all_names.len; i++){
		if(path_str == all_names.allNames[i].name){   //found name
			foundName = 1;
			index = all_names.allNames[i].inode;
			all_names.allNames[i].file_size = content.length();
			break;
		}
	}
	if(foundName == 0)
		throw std::runtime_error("file not exist!");

	
	if(content.length() > 512){
		inodeTable.table[index].numBlocks = content.length() / 512;
		if(content.length() % 512 != 0)
			inodeTable.table[index].numBlocks++;
	}
	std::string partOfContent;
	std::string org_content = content;
	int addr;
	//pregmention:
	for(int i = 0; i < inodeTable.table[index].numBlocks; i++){
		addr = inodeTable.table[index].blocks[i] * 512;	inodeTable;
		if(content.length() > 512){
			partOfContent = content.substr(0, 512);
			content = content.substr(512 + 1, content.length());
		}
		else
			partOfContent = content;
		const char *cstr = partOfContent.c_str();
		blkdevsim->write(addr, org_content.length(), cstr);    //write content to root
	}

	//int addr = inodeTable.table[index].blocks[0] * 512;	
	//const char *cstr = content.c_str();
	//blkdevsim->write(addr, sizeof(cstr), cstr);    //write content to root

	blkdevsim->write(base_inode_table, sizeof(inodeTable), (const char*)&inodeTable);    //update inode table
	blkdevsim->write(base_names, sizeof(all_names), (const char*)&all_names);    //update names

}	


MyFs::dir_list MyFs::list_dir(std::string path_str) {
	dir_list listFiles;
	for(int i = 0; i < all_names.len; i++){
		listFiles.push_back(all_names.allNames[i]);
	}
	return listFiles;
}

//function updates data when file start runing
int MyFs::getFilesData()
{
	blkdevsim->read(base_names, sizeof(all_names), (char*)&all_names);  //read names
	blkdevsim->read(base_inode_table, sizeof(inodeTable), (char*)&inodeTable);    //read inode table
}


//function scaning the inode table and returns the first empy block
int MyFs::getFirstEmpty()
{
	char block_is_empty;   //0- empty, 1- not empty
	for(int i = base_data; i < base_data + 20 * 512; i+=512){
		blkdevsim->read(i, 1, (char*)&block_is_empty);
		if(block_is_empty == 0){
			return i;
		}
	}
}